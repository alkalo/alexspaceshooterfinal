﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour {


	public float timeLeft = 0;
	public Cannon cannon;
	//Create bullets
	public int maxBullets;
	public Transform bulletPosition;
	public Cartridge[] cart;
	public int currentCartridge = 0;
	public int cartn = 0;



	// Use this for initialization
	void Start () {
		GameObject[] bullets = Resources.LoadAll<GameObject> ("Prefabs/Bullets");

		cart = new Cartridge[bullets.Length];

		Vector2 spawnpos = bulletPosition.position;

		for (int i = 0; i < bullets.Length; i++) {
			cart[i] = new Cartridge(bullets[i], bulletPosition, spawnpos, maxBullets);
			spawnpos.y += 1;
		}

	}
	void Update(){
		timeLeft -= Time.deltaTime;
		if (timeLeft <= 0) {
			currentCartridge = 0;
		}
	}

	public void Shoot(){
		cannon.ShootCannon (cart[currentCartridge]);
	}
		
	public void ChangeCartridge1(){
		timeLeft = 5f; 
		currentCartridge = 1;

	}
	public void ChangeCartridge2(){
		timeLeft = 5f; 
		currentCartridge = 3;
	}
	public void ChangeCartridge3(){
		timeLeft = 5f; 
		currentCartridge = 4;

	}

}
