﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DobleBulletParalel: Bullet {
	public Cartridge cart;

	// Use this for initialization
	void Awake () {
		cart = GameObject.Find("Weapons").GetComponent<Weapons>().cart [0];
	}
    

	public override void Shot(Vector3 position, float direction){
		//LEFT SHOT
		Vector3 left = new Vector3 (position.x,position.y,position.z);
		left.x -= 0.2f;
		cart.GetBullet ().Shot (left, direction);

		//RIGHT SHOT
		Vector3 right = new Vector3 (position.x,position.y,position.z);
		right.x += 0.2f;

		cart.GetBullet ().Shot (right, direction);

	}
}
