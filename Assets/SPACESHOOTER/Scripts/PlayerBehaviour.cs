﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour {
	public float speed;
	private Vector2 axis;
	public Vector2 limits;
	public Propeller propeller;

	public GameObject graphics;
	public BoxCollider2D myCollider;
	public AudioSource audios;
	public ParticleSystem explosion;
	public GameObject canvasgameover;
	public GameObject spaceShooter;


	void Awake(){
		spaceShooter.SetActive (true);
		myCollider = GetComponent<BoxCollider2D> ();
	}
	// Update is called once per frame
	void Update () {
		transform.Translate (axis * speed * Time.deltaTime);

		if (transform.position.x > limits.x) {
			transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
		}else if (transform.position.x < -limits.x) {
			transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
		}

		if (transform.position.y > limits.y) {
			transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
		}else if (transform.position.y < -limits.y) {
			transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
		}

		if (axis.y > 0) {
			propeller.RedFire ();
		} else if (axis.y < 0) {
			propeller.BlueFire ();
		} else {
			propeller.Stop ();
		}




	}

	public void SetAxis(Vector2 currentAxis){
		axis = currentAxis;
	}

	public void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "meteor")
			Explode ();
		if (col.tag == "EnemyBullet") {
			Explode ();
		}
	}

	private void Explode(){

		MyGameManager.getInstance ().LoseLive ();
		graphics.SetActive(false);
		myCollider.enabled = false;
		explosion.Play ();
		audios.Play ();
		if (MyGameManager.getInstance().lives <= 0){
			GameOver ();
		}
		Invoke ("Reset",2);
	}

	public void Reset(){
		graphics.SetActive(true);
		myCollider.enabled = true;

	}
	public void GameOver(){
		canvasgameover.SetActive(true);
		spaceShooter.SetActive (false);
	}




}
