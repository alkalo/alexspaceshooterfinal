﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public float speed;
	public bool shooting = true;
	private Vector3 iniPos;
	public float offset = 0.0f;

	void Start(){
		iniPos = transform.position;
	}

	public virtual void Shot(Vector3 position, float direction){
		transform.position = position;
		offset = direction;
		shooting = true;
		transform.rotation = Quaternion.Euler(0f, 0f, offset);
	}

	// Update is called once per frame
	protected void Update () {
		if (shooting) {
			transform.Translate (0, speed * Time.deltaTime, 0);
		}
	}

	public void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Finish" || other.tag == "meteor") {
			Reset ();	
		}

	}

	public void Reset(){
		transform.position = iniPos;
		shooting = false;
	}
}