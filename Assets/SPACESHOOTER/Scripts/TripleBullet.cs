﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleBullet : Bullet {
	public float timeFrag = 1.0f;
	public Cartridge cart;
	private float currentTime = 0.0f;

	// Use this for initialization
	void Awake () {
		cart = GameObject.Find("Weapons").GetComponent<Weapons>().cart [0];
	}
	
	protected new virtual void Update () {
		base.Update ();
		if (shooting) {
			currentTime += Time.deltaTime;
			if (currentTime >= timeFrag) {
				MultiShoot ();
			}
		}
	}

	public void MultiShoot(){
		cart.GetBullet ().Shot (transform.position, -45f);
		cart.GetBullet ().Shot (transform.position, 0.0f);
		cart.GetBullet ().Shot (transform.position, 45f);
		currentTime = 0.0f;
		Reset();
	}
}
