﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsController : MonoBehaviour {

	// Use this for initialization
	public void FullScreen(){
		Screen.fullScreen = !Screen.fullScreen;
	}
}
