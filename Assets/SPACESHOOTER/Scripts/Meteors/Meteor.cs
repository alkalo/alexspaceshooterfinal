﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{

	public bool launched;
	public Vector2 moveSpeed;
	protected float rotationSpeed;

	public GameObject graphicsMeteor;
	public ParticleSystem ps;
	private Collider2D meteorCollider;

	private Transform graphics;

	//Sounds
	public AudioSource explosion;




	public Vector3 iniPos;

	void Start()
	{
		iniPos = transform.position;
		meteorCollider = GetComponent<Collider2D> ();
		graphics = transform.GetChild (0);
	}

	public void LaunchMeteor(Vector3 position, Vector2 direction, float rotation)
	{
		transform.position = position;

		//Rotation del meteor
		launched = true;
		transform.rotation = Quaternion.Euler(0, 0, 0);

		moveSpeed = direction;
		rotationSpeed = rotation;

	}

	// Update is called once per frame
	protected void Update()
	{
		if (launched)
		{
			transform.Translate(moveSpeed.x * Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
			graphics.Rotate (0,0,rotationSpeed);

		}
	}

	public virtual void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Bullet")
		{
			Explode ();
		}
		if (other.tag == "Finish")
		{
			Reset();
		}
		if (other.tag == "Player")
		{
			Explode ();	
		}
	}

	protected void Reset()
	{
		transform.position = iniPos;
		launched = false;
		graphicsMeteor.SetActive (true);
		meteorCollider.enabled = true;
	}

	public virtual void Explode(){
		launched = false;
		MyGameManager.getInstance ().AddScore (50);
		graphicsMeteor.SetActive (false);
		meteorCollider.enabled = false;
		ps.Play ();
		Invoke ("Reset",1);
		explosion.Play ();	
	}
}