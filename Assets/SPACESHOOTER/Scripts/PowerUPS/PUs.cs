﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUs : MonoBehaviour
{

	public bool launched;
	public Vector2 moveSpeed;
	protected float rotationSpeed;

	public GameObject graphicsPus;
	public ParticleSystem ps;
	private Collider2D pusCollider;

	public Weapons weaponsS;





	private Transform graphics;

	//Sounds
	public AudioSource explosion;

	//INTENTO NUMERO 1





	public Vector3 iniPos;

	void Start()
	{

		GameObject weapons = GameObject.Find ("Weapons");
		weaponsS = weapons.GetComponent<Weapons> (); 

		iniPos = transform.position;
		pusCollider = GetComponent<Collider2D> ();
		graphics = transform.GetChild (0);

	}

	public void LaunchPus(Vector3 position, Vector2 direction, float rotation)
	{
		transform.position = position;

		//Rotation del enemy
		launched = true;
		transform.rotation = Quaternion.Euler(0, 0, 0);

		moveSpeed = direction;
		rotationSpeed = rotation;

	}

	// Update is called once per frame
	protected void Update()
	{
		if (launched)
		{
			transform.Translate(moveSpeed.x * Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
			graphics.Rotate (0,0,rotationSpeed);

		}



	}

	public virtual void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Bullet"){
			Reset ();
		}
		if (other.tag == "Player")
		{
			Explode ();   
			Reset ();
			ChangeWeapon1 ();
		}
	}

	protected void Reset()
	{
		transform.position = iniPos;
		launched = false;
		graphicsPus.SetActive (true);
		pusCollider.enabled = true;
	}

	public virtual void Explode(){
		launched = false;
		MyGameManager.getInstance ().AddScore (50);
		graphicsPus.SetActive (false);
		pusCollider.enabled = false;
		ps.Play ();
		Invoke ("Reset",1);
		explosion.Play ();    
	}

	public void ChangeWeapon1(){
		weaponsS.ChangeCartridge1 ();
	}

}
