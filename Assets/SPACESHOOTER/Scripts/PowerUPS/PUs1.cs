﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUs1 : PUs
{

	public void ChangeWeapon2(){
		weaponsS.ChangeCartridge2 ();
	}

	public override void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Bullet"){
			Reset ();
		}
		if (other.tag == "Player")
		{
			Explode ();   
			Reset ();
			ChangeWeapon2 ();
		}
	}

}
