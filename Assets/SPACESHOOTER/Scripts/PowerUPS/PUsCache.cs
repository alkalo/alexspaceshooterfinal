﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUsCache
{
	private PUs[] pus;
	private int currentPus = 0;


	public PUsCache(GameObject prefabPus, Vector3 position, Transform parent, int numPus)
	{
		pus = new PUs[numPus];

		Vector3 tmpPosition = position;
		for (int i = 0; i < numPus; i++)
		{
			pus[i] = GameObject.Instantiate(prefabPus, tmpPosition, Quaternion.identity, parent).GetComponent<PUs>();
			pus[i].name = prefabPus.name + "_pus_" + i;
			tmpPosition.x += 1;
		}
	}

	public PUs GetPus()
	{
		if (currentPus > pus.Length - 1)
		{
			currentPus = 0;
		}

		return pus[currentPus++];
	}
}
