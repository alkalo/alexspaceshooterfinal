﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUsManager : MonoBehaviour {

	public Transform launchPos;
	public float xVariation;
	public Transform creationPos;
	public float timeLaunch;


	public GameObject[] pusPrefabs;
	private PUsCache[] pusCaches;
	private float currentTime;


	private static PUsManager instance;

	public static PUsManager getInstance(){
		return instance;
	}




	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
		}

		Vector3 creationPosMeteor = creationPos.position;
		pusCaches = new PUsCache[pusPrefabs.Length];

		for (int i = 0; i < pusPrefabs.Length; i++) {
			//Big Meteors;
			pusCaches[i] = new PUsCache (pusPrefabs[i], creationPosMeteor, creationPos, 30);

			creationPosMeteor.y += 1;
		}
	}

	void Update(){
		currentTime += Time.deltaTime;

		if (currentTime > timeLaunch) {
			pusCaches [Random.Range(0,pusCaches.Length)].GetPus ().LaunchPus (launchPos.position, new Vector2 (Random.Range(-5, 5), Random.Range(-5, -1)),Random.Range(-1,1));
			currentTime -= timeLaunch;
		}
	}

	public void LaunchMeteor(int type, Vector3 position, Vector2 direction, float rotation){
		pusCaches [type].GetPus ().LaunchPus(position, direction, rotation);
	}

}
