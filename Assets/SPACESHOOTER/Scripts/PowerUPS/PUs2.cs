﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUs2 : PUs
{

	public void ChangeWeapon3(){
		weaponsS.ChangeCartridge3 ();
	}

	public override void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Bullet"){
			Reset ();
		}
		if (other.tag == "Player")
		{
			Explode ();   
			Reset ();
			ChangeWeapon3 ();
		}
	}

}
