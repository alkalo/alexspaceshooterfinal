﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour {

	public Vector3 bulletOffset = new Vector3(0,0.5f,0);

	public GameObject bulletPrefab;


	public float fireDelay;
	float coldownTimer = 0;
	bool dispara = false;
	
	// Update is called once per frame
	void Update () {
		coldownTimer += Time.deltaTime;

		if (coldownTimer >= fireDelay) {
			coldownTimer -= fireDelay;

			Vector3 offset = transform.rotation * bulletOffset;
			if (dispara) {
				GameObject bulletGO = Instantiate (bulletPrefab, transform.position + offset, transform.rotation);
				BulletGOO bg = bulletGO.GetComponent<BulletGOO> ();
				bg.Shot (transform.position);
			}
				
			

				
		}
	}

	public void Eshooting(){
		dispara = true;
	}



}
