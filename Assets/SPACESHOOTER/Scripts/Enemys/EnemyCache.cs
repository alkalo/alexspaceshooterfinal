﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCache
{
	private Enemy[] enemys;
	private int currentEnemy = 0;


	public EnemyCache(GameObject prefabEnemy, Vector3 position, Transform parent, int numEnemys)
	{
		enemys = new Enemy[numEnemys];

		Vector3 tmpPosition = position;
		for (int i = 0; i < numEnemys; i++)
		{
			enemys[i] = GameObject.Instantiate(prefabEnemy, tmpPosition, Quaternion.identity, parent).GetComponent<Enemy>();
			enemys[i].name = prefabEnemy.name + "_enemy_" + i;
			tmpPosition.x += 1;
		}
	}

	public Enemy GetEnemy()
	{
		if (currentEnemy > enemys.Length - 1)
		{
			currentEnemy = 0;
		}

		return enemys[currentEnemy++];
	}
}
