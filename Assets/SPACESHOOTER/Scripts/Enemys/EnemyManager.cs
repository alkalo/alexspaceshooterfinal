﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

	public Transform launchPos;
	public float xVariation;
	public Transform creationPos;
	public float timeLaunch;


	public GameObject[] enemyPrefabs;
	private EnemyCache[] enemyCaches;
	private float currentTime;

	private static EnemyManager instance;

	public static EnemyManager getInstance(){
		return instance;
	}




	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
		}

		Vector3 creationPosMeteor = creationPos.position;
		enemyCaches = new EnemyCache[enemyPrefabs.Length];

		for (int i = 0; i < enemyPrefabs.Length; i++) {
			//Big Meteors;
			enemyCaches[i] = new EnemyCache (enemyPrefabs[i], creationPosMeteor, creationPos, 30);

			creationPosMeteor.y += 1;
		}
	}

	void Update(){
		currentTime += Time.deltaTime;

		if (currentTime > timeLaunch) {
			enemyCaches [Random.Range(0,enemyCaches.Length)].GetEnemy ().LaunchEnemy (launchPos.position, new Vector2 (Random.Range(-5, 5), Random.Range(-5, -1)),0);
			currentTime -= timeLaunch;
		}
	}

	public void LaunchMeteor(int type, Vector3 position, Vector2 direction, float rotation){
		enemyCaches [type].GetEnemy ().LaunchEnemy(position, direction, rotation);
	}

}
