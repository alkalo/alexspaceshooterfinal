﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletGOO : MonoBehaviour {
	public float speed;
	private bool canshoot = false;
	private Vector3 iniPos;




	// Use this for initialization
	void Start () {
		iniPos = transform.position;

	}
	public void Shot(Vector3 position){
		transform.position = position;
		canshoot = true;

	}
	
	// Update is called once per frame
	void Update () {
		if (canshoot == true) {
			transform.Translate (0,-speed*Time.deltaTime,0);
		}

	}

	public void Reset(){
		transform.position = iniPos;
		canshoot = false;
	}

	public void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Finish" || col.tag == "meteor") {
			Destroy (gameObject);
		}
	}


}
