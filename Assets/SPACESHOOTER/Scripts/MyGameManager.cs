﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyGameManager : MonoBehaviour {


	public Text scoreText;
	public Text scoreTextGO;
	public int score;
	public Text livesText;
	public int lives;


	private static MyGameManager instance;

	void Awake(){
		if (instance == null)
			instance = this;
	}

	public static MyGameManager getInstance(){
		return instance;
	}

	// Use this for initialization
	void Start () {
		lives = 3;
		score = 0;
		scoreText.text = score.ToString ("D5");
		scoreTextGO.text = score.ToString ("D5");
		livesText.text = "x" + lives.ToString ();
	}


	public void LoseLive(){
		lives--;
		livesText.text = "x" + lives.ToString ();
	}

	public void AddScore(int value){
		score += value;
		scoreText.text = score.ToString ("D5");
		scoreTextGO.text = score.ToString ("D5");
	
	}
}
