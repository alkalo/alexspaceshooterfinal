﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManagert : MonoBehaviour {

	public void StartGame(){
		SceneManager.LoadScene ("SpaceShooter");	
	}

	public void FullScreen(){
		Screen.fullScreen = !Screen.fullScreen;
	}
}
